## Name
# Sashiko Inkscape Extension

## Description
Sashiko is a style of Japanese hand embroidery.  This is an [Inkscape](https://inkscape.org/) 
extension that will create 
Japanese [Sashiko](https://en.wikipedia.org/wiki/Sashiko) stitching patterns.
 I find the patterns beautiful.
I was asked to draw some of them in Inkscape. I decided this would be a great
way for me to learn to make an extension in Inkscape. 

If you plan to use these patterns for laser cutting be aware that some of the 
patterns have overlapping paths. This is not an issue for laser printing.

## Example
Here is the kind of output this extension will create.

![Example of output from the Chained Crosses pattern.](photos/chained_crosses.png)

Chained Crosses.png

## Installation
To add this extension to Inkscape, so it can be used, you will need to copy 
two files to Inkscape's user extensions folder.
sashiko.inx and sashiko.py are the two files. 
Copy these from the repository.

To find the folder to copy them to:
1. Open Inkscape.
2. Go to Edit->Preferences->System
3. Under 'System info' find the 'User Extensions' field
4. Click 'Open' next to 'User Extensions'
5. Copy the two files to the folder.
6. Restart Inkscape

## Usage
In Inkscape 1.1 under the Extensions pulldown menu choose Render->Sashiko...

Choose a pattern and the number of rows and columns for the pattern to be repeated.


## Support
Submit issues at the Gitlab repository or email me with questions.

## Roadmap
I plan to fix the overlapping lines issue that a few of the patterns have. 
This will only affect those who intend to use the patterns on a laser cutter or CNC router.

## Contributing
Contact me if you would like to contribute code or patterns.

If you are a Python programmer and would like to create more patterns you can. 
To add your own patterns you will need to draw a base tile in Inkscape that can be
repeated in columns and rows. Copy the paths from the SVG file from that drawing
and add them into sashiko.py at the appropriate place.

Non-programmers can contact me with a pattern to be included.

Let me know of feature requests also. I have limited time but if I like the
idea, and it is in my skill level I will implement it.

## Related links
[Here is a blog](https://kate-ward-design.blogspot.com/2018/05/how-to-print-sashiko-stencils-on-fabric.html)
that explains how to print patterns directly onto fabric.

[The upcyclestitches](https://upcyclestitches.com/) webpage has a lot of good information,
videos, and books about Boro & Sashiko.

## Authors and acknowledgment
I was inspired by the book [Boro & Sashiko, Harmonious Imperfection](https://www.youtube.com/watch?v=HiG5qBPDN9A)
by Shannon & Jason Mullett-Bowlsby, the Shibaguyz.

Thanks to [George Zhang](https://github.com/georgexyz19) 
for his excellent [Inkscape Extensions Development Tutorial](https://inkscapetutorial.org/pages/extension.html).

## License
This project is licensed under GNU GPL V3. Refer to [the license file](LICENSE).

## Project status
The initial version is complete. I'm looking for Beta testers or anyone willing
to give feedback.
